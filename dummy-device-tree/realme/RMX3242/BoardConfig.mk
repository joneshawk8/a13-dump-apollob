DEVICE_PATH := device/realme/RMX3242
BOARD_VENDOR := realme

# Security patch level
VENDOR_SECURITY_PATCH := 2023-09-05

DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := $(DEVICE_PATH)/framework_compatibility_matrix.xml

-include vendor/realme/RMX3242/BoardConfigVendor.mk